$(document).ready(function() {
    /*næ í gögn fyrir lottó*/
    $.ajax({
        'url': 'http://apis.is/lottery',
        'type': 'GET',
        'dataType': 'json',
        /*Set gögn inn í handlebars og birti*/
        'success': function(response) {
            console.log(response);
            var template = $("#lottoTemplate").html();
            var renderer = Handlebars.compile(template);
            var results = renderer(response);

            $("#lotto").html(results);
        }
    });
    /*næ í gögn fyrir víkingalottó*/
    $.ajax({
        'url': 'http://apis.is/lottery/vikingalotto',
        'type': 'GET',
        'dataType': 'json',
        /*Set gögn inn í handlebars og birti*/
        'success': function(response) {
            console.log(response);
            var template = $("#vklottoTemplate").html();
            var renderer = Handlebars.compile(template);
            var results = renderer(response);

            $("#vklotto").html(results);
        }
    });
    /*næ í gögn fyrir eurojackpot*/
    $.ajax({
        'url': 'http://apis.is/lottery/eurojackpot',
        'type': 'GET',
        'dataType': 'json',
        /*Set gögn inn í handlebars og birti*/
        'success': function(response) {
            console.log(response);
            var template = $("#ejTemplate").html();
            var renderer = Handlebars.compile(template);
            var results = renderer(response);

            $("#eurojackpot").html(results);
        }
    });
}) ;
